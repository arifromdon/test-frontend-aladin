import React, { useState, useEffect } from 'react'
import API from '../../../.././utils/API'
import iconStar from '../../../../styles/images/ic-star.svg'
import iconDot from '../../../../styles/images/ic-dot.svg'
import Slider from "react-slick"

const BannerSlider = () => {
  const [trendingMovies, setTrendingMovies] = useState([]);

  useEffect(() => {
    getDataTrending()
  }, [])

  const getDataTrending = async () => {
    try {
      const response = await API.get(`${process.env.REACT_APP_BASE_URL}trending/movie/day?api_key=${process.env.REACT_APP_API_KEY}`)
      if (response.data.results) {
        setTrendingMovies(response.data.results)
        getGenresMovie(response.data.results)
      }
    } catch(e) {
      setTrendingMovies([])
    }
  }

  const getGenresMovie = async (dataMovies) => {
    try {
      const response = await API.get(`${process.env.REACT_APP_BASE_URL}genre/movie/list?api_key=${process.env.REACT_APP_API_KEY}&language=en-US`)
      const finding = []
      dataMovies.filter(item => {
        response.data.genres.filter(data => {
          if (data.id === item.genre_ids[0]) {
            finding.push({ ...item, genre_name: data.name })
          }
        })
      })
      setTrendingMovies([...trendingMovies, ...finding])
    } catch(e) {
      setTrendingMovies([])
    }
  }

  const settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 3,
    slidesToScroll: 1,
    className: "center",
    centerMode: true,
    centerPadding: "-50px"
  }

  return (
    <div className="wrapper-slider">
      <Slider {...settings}>
        {
          trendingMovies.map(item => (
            <div key={Math.random()}>
              <div className="d-flex-align-center">
                <img src={`https://image.tmdb.org/t/p/original/${item.poster_path}`} className="img-cover" alt={`${Math.random()}-poster`} />
                <div className="card-slider">
                  <div className="d-flex-align-center">
                    <img src={iconStar} className="icon-star" alt="icon star" />
                    <span>{item.vote_average}</span>
                  </div>
                  <h3>{item.title}</h3>
                  <div className="d-flex-align-center section-type">
                    <p>{item.release_date.split('-')[0]}</p>
                    <img src={iconDot} alt="icon dot" />
                    <p>{item.genre_name}</p>
                  </div>
                  <p className="description">{item.overview}</p>
                </div>
              </div>
            </div>
          ))
        }
      </Slider>
    </div>
  )
}

export default BannerSlider
