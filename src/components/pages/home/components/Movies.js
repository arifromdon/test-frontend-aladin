import React, { useState, useEffect } from 'react'
import API from '../../../.././utils/API'
import CardMovie from "../../../globals/components/CardMovie"

const MovieList = () => {
  const [movieList, setMovieList] = useState([])
  const [metaMovies, setMetaMovies] = useState({
    page: 1
  })
  const [genreMovies, setGenreMovies] = useState([])
  const [pickSort, setPickSort] = useState('')
  const [sorting, setSorting] = useState([
    {
      name: 'Popularity Ascending',
      value: 'popularity.asc'
    },
    {
      name: 'Popularity Descending',
      value: 'popularity.desc'
    },
    {
      name: 'Release Date Ascending',
      value: 'release_date.asc'
    },
    {
      name: 'Release Date Descending',
      value: 'release_date.desc'
    },
    {
      name: 'Rating Ascending',
      value: 'rating.asc'
    },
    {
      name: 'Rating Descending',
      value: 'rating.desc'
    }
  ])

  useEffect(() => {
    getMovieList()
  }, [])

  const getMovieList = async (pages, sorting) => {
    try {
      const response = await API.get(`${process.env.REACT_APP_BASE_URL}discover/movie?sort_by=${sorting ? sorting : ''}&api_key=${process.env.REACT_APP_API_KEY}&page=${pages ? pages : 1}`)
      if (response.data) {
        setMovieList(movieList && (pages !== metaMovies.page) ? [ ...movieList, ...response.data.results] : response.data.results)
        setMetaMovies(response.data)
        getGenresMovie(response.data.results)
      }
    } catch {
      setMovieList(null)
    }
  }

  const getGenresMovie = async (dataMovies) => {
    try {
      const response = await API.get(`${process.env.REACT_APP_BASE_URL}genre/movie/list?api_key=${process.env.REACT_APP_API_KEY}&language=en-US`)
      const finding = []
      dataMovies.filter(item => {
        response.data.genres.filter(data => {
          if (data.id === item.genre_ids[0]) {
            finding.push({ ...item, genre_name: data.name })
          }
        })
      })
      setMovieList([...movieList, ...finding])
      setGenreMovies(response.data.genres)
    } catch {
      setGenreMovies([])
    }
  }

  const handleLoadMore = () => {
    getMovieList(metaMovies.page + 1)
  }

  const handleSorting = (data) => {
    getMovieList(metaMovies.page, data.value)
    setPickSort(data.name)
  }

  return (
    <div className="wrapper-movie-list">
      <div className="bg-top-movie-list"></div>
      <div className="container">
        <div className="row">
          <div className="col-12 d-flex-align-center-between mt-5">
            <div>
              <div className="badge-line"></div>
              <h4>Discover Movies</h4>
            </div>
            <div className="d-flex-align-center-between">
              <span className="me-3 my-movies">My Movies</span>
              <div className="badge-main">
                <strong className="me-2">2</strong>
                <span>movies</span>
              </div>
            </div>
          </div>
          <div className="col-12">
            <div className="row">
              <div className="col-2">
                <div className="wrapper-sorting">
                  <p>Sort Result By</p>
                  <hr />
                  <div className="dropdown">
                    <button className="dropbtn truncate">
                      {pickSort ? pickSort : 'Sort By'}
                    </button>
                    <div className="dropdown-content">
                    {
                      sorting.map(item => (
                        <div
                          key={Math.random()}
                          className="item-sorting"
                          onClick={() => handleSorting(item)}
                        >
                          {item.name}
                        </div>
                      ))
                    }
                    </div>
                  </div>
                  <hr />
                  <p>Genres</p>
                  <hr />
                  <div className="genre-list">
                    {
                      genreMovies.map(item => (
                        <div
                          key={Math.random()}
                          className="d-flex-align-center-between mb-2"
                        >
                          <label htmlFor={item.name}>{item.name}</label>
                          <input type="checkbox" id={item.id} name={item.name} value={item.name} />
                        </div>
                      ))
                    }
                  </div>
                </div>
              </div>
              <div className="col-10">
                <div className="row">
                  {
                    movieList ? movieList.map(item => (
                      <div key={Math.random()} className="col-3 mb-3">
                        <CardMovie dataMovie={item} />
                      </div>
                    )) : (
                      <div key={Math.random()} className="col-3 mb-3">
                        <h4>Data Not Found</h4>
                      </div>
                    )
                  }
                </div>
                <div className="row">
                  <div className="col-12 d-flex-align-middle-center my-4">
                    <button
                      className="btn btn-primary"
                      type="button"
                      onClick={() => handleLoadMore()}
                    >Load More</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default MovieList
