import NavbarComponent from "../../globals/components/Navbar"
import BannerSlider from "./components/Slider"
import MovieList from "./components/Movies"
import FooterComponent from "../../globals/components/Footer"

const HomePage = () => {
  return (
    <div>
      <NavbarComponent />
      <BannerSlider />
      <MovieList />
      <FooterComponent />
    </div>
  )
}

export default HomePage
