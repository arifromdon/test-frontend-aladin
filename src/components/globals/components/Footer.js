import logoGrey from '../../../styles/images/moovietime-logo-grey.svg';

const Footer = () => (
  <div className="wrapper-footer">
    <div className="container">
      <div className="row">
        <div className="col-4">
          <span>
            ©  2021 MoovieTime. All rights reserved.
          </span>
        </div>
        <div className="col-4">
          <img src={logoGrey} alt="icon logo" />
        </div>
        <div className="col-4">
          <span>
            Made with React JS
          </span>
        </div>
      </div>
    </div>
  </div>
)

export default Footer