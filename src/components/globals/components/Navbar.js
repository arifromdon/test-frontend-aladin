import logo from '../../../styles/images/moovietime-logo.svg';
import gridIcon from '../../../styles/images/grid-icon.svg';

const NavbarComponent = () => {
  return (
    <div id="myTopnav" className="topnav">
      <div className="container">
        <div className="row">
          <div className="col-6 d-flex-align-center">
            <a href="#home" className="active">
              <img src={logo} alt="icon logo" />
            </a>
            <input type="text" placeholder="Find movie" />
          </div>
          <div className="col-5 d-flex-align-center-between">
            <a href="#news" className="d-flex-align-center">
              <img src={gridIcon} alt="icon grid" />
              <span>CATEGORIES</span>
            </a>
            <a href="#contact">MOVIES</a>
            <a href="#about">TV SHOWS</a>
            <a href="#about">LOGIN</a>
          </div>
        </div>
      </div>
    </div>
  )
}

export default NavbarComponent
