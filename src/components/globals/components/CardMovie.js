import iconStar from '../../../styles/images/ic-star.svg'

const CardMovie = (data) => {
  return (
    <div className="card-movie">
      <div className="wrapper-overview">
        <div className="overview">
          <img
            src={`https://image.tmdb.org/t/p/original/${data.dataMovie.poster_path}`}
            className="img-cover"
            alt="poster-movie"
          />
          <span className="vote-average">{data.dataMovie.vote_average}</span>
        </div>
        <div className="hover-overview">
          <div className="wrapper-hover">
            <div className="d-flex-align-middle-center">
              <img src={iconStar} className="icon-star" alt="icon star" />
              <span>{data.dataMovie.vote_average}</span>
            </div>
            <h4>{data.dataMovie.genre_name}</h4>
            <div>
              <button
                className="btn btn-primary"
                type="button"
              >VIEW</button>
              <button
                className="btn btn-secondary"
                type="button"
              >ADD</button>
            </div>
          </div>
        </div>
      </div>
      <p className="truncate">{data.dataMovie.title}</p>
      <span>{data.dataMovie.release_date.split('-')[0]}</span>
    </div>
  )
}

export default CardMovie
